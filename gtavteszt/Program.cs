﻿using System;
using System.Windows.Forms;
using GTA;
using GTA.Native;

namespace GTAVCleanMap
{
    public class Main : Script
    {
        // Author: Xenius 2015.12.13. //
        public static bool modEnabled = false;
        public Main()
        {
            Tick += OnTick;
            KeyDown += OnKeyDown;

            UI.Notify("Clean Map v0.1 loaded! Toggle: F10 | by Xenius");
        }
        
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10)
            {
                if (modEnabled)
                {
                    modEnabled = false;
                    UI.Notify("Clean Map disabled!");
                }
                else
                {
                    modEnabled = true;
                    UI.Notify("Clean Map enabled!");
                }
            }
        }

        public static void clearThisFrame()
        {
            if (modEnabled)
            {
                var pos = Game.Player.Character.Position;
                Function.Call(Hash.CLEAR_AREA_OF_PEDS, pos.X, pos.Y, pos.Z, 10000f, 0);
                Function.Call(Hash.CLEAR_AREA_OF_VEHICLES, pos.X, pos.Y, pos.Z, 10000f, 0);

                Function.Call(Hash.SET_GARBAGE_TRUCKS, 0);
                Function.Call(Hash.SET_RANDOM_BOATS, 0);
                Function.Call(Hash.SET_RANDOM_TRAINS, 0);
                Function.Call(Hash.DELETE_ALL_TRAINS);

                Function.Call(Hash.SET_TIME_SCALE, 1f);

                Function.Call(Hash.SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
                Function.Call(Hash.SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
                Function.Call(Hash.SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
                Function.Call(Hash.SET_PED_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
                Function.Call(Hash.SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME, 0f, 0f);
            }
        }
        
        public void OnTick(object sender, EventArgs e)
        {
            clearThisFrame();
        }
    }
}
